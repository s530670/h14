# FOR EACH VIDEO, COUNT COMMENTS.
# SORTED DATA HAS 1 COMMENT PER LINE, ALL VIDEOIDS TOGETHER
# 
# STEP 3. REDUCE videoID and counts by summing the counts into 1 record per video

# open s.csv for reading as a file stream named s
# open r.csv for writing as a file stream named r
s = open("s.csv","r")
r = open("r.csv","w")



# initialize thisKey to an empty string
thisKey = ''

# initialize thisValue to 0
thisValue = 0

# output column headers
r.write('videoID,comments\n')

# for every line in s
for line in s:

  # use the line strip and split methods and assign to a list named data
  data = line.strip().split(',')

  # assign data list to named variables id and comments
  id,comments = data

  # if  id is not equal to thisKey
  if id != thisKey:

    # if thisKey
    if thisKey:
      # output the last key value pair result
      r.write(thisKey + ',' + str(thisValue)+'\n')
    
    # start over when changing keys - initialize thisKey to id and thisValue to 0 
    thisKey = id
    thisValue = 0

  # apply the aggregation function  
  thisValue += int(comments)


# output the final entry when done
r.write(thisKey + ',' + str(thisValue)+'\n')

# close the file streams
r.close()
s.close()